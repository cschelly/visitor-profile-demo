//
//  TealiumHelper.swift
//  TealiumVisitorProfileDemo
//
//  Created by Christina S on 11/7/19.
//  Copyright © 2019 Christina. All rights reserved.
//

import Foundation
import TealiumSwift

enum TealiumConfiguration {
    static let account = "tealiummobile"
    static let profile = "demo"
    static let environment = "dev"
    static let datasource = "test123" // UDH data source key
}

enum TealiumSampleAudiences: String {
    case cartabandoners = "Cart Abandoners",
    travelers = "Frequent Travelers",
    highscorers = "High Scoring Gamers"
}

// Note: update this to false if you want to disable logging
let enableLogs = true

class TealiumHelper {

    static let shared = TealiumHelper()

    let config = TealiumConfig(account: TealiumConfiguration.account,
                               profile: TealiumConfiguration.profile,
                               environment: TealiumConfiguration.environment,
                               datasource: TealiumConfiguration.datasource)

    var tealium: Tealium?

    private init() {
        if enableLogs { config.setLogLevel(.verbose) }
        let list = TealiumModulesList(isWhitelist: false,
                                      moduleNames: ["autotracking",
                                                    "tagmanagement",
                                                    "consentmanager"])
        config.setModulesList(list)
        config.setDiskStorageEnabled(isEnabled: true)
        tealium = Tealium(config: config, enableCompletion: { [weak self] response in
            guard let self = self else {
                return
            }
            self.tealium?.visitorService()?.addVisitorServiceDelegate(self)
        })
        
        tealium?.track(title: "tealium_initialized")
    }


    public func start() {
        _ = TealiumHelper.shared
    }

    class func trackView(title: String, data: [String: Any]?) {
        TealiumHelper.shared.tealium?.track(title: title, data: data, completion: nil)
    }

    class func trackScreen(_ view: UIViewController, name: String) {
        TealiumHelper.trackView(title: "screen_view",
                                     data: ["screen_name": name,
                                            "screen_class": "\(view.classForCoder)"])
    }

    class func trackEvent(title: String, data: [String: Any]?) {
        TealiumHelper.shared.tealium?.track(title: title, data: data, completion: nil)
    }
    
    class func joinTrace(traceId: String) {
        TealiumHelper.shared.tealium?.track(title: "trace",
                                                 data: ["tealium_trace_id": traceId],
                                                 completion: nil)
    }
    
    class func leaveTrace() {
        TealiumHelper.shared.tealium?.leaveTrace()
    }
    
    class func killTrace(traceId: String) {
        TealiumHelper.shared.tealium?.track(title: "kill_trace",
                                                 data: ["event":"kill_visitor_session",
                                                        "cp.trace_id": traceId],
                                                 completion: nil)
    }
    
    class func addVisitorServiceDelegate(_ delegate: TealiumVisitorServiceDelegate) {
        TealiumHelper.shared
            .tealium?.visitorService()?.addVisitorServiceDelegate(delegate)
    }
    
    class func getCachedAudeinces(_ completion: @escaping (Result<Audiences, TealiumError>) -> Void) {
        TealiumHelper.shared
            .tealium?.visitorService()?.getCachedProfile(completion: { profile in
                guard let profile = profile,
                    let audiences = profile.audiences else {
                    completion(.failure(.noAudiencesReturned))
                    return
                }
                completion(.success(audiences))
        })
    }
    
    class func updateExperience(basedOn audience: TealiumSampleAudiences, _ experienceUpdate: @escaping () -> Void) {
        TealiumHelper.getCachedAudeinces { result in
        switch result {
            case .success(let audiences):
                if audiences[name: audience.rawValue] {
                    DispatchQueue.main.async {
                        experienceUpdate()
                    }
                }
            case .failure(let error):
                print("Error getting audiences: \(error)")
            }
        }
    }

}

extension TealiumHelper: TealiumVisitorServiceDelegate {
    func profileDidUpdate(profile: TealiumVisitorProfile?) {
        guard let profile = profile else {
            return
        }
        if let json = try? JSONEncoder().encode(profile), let string = String(data: json, encoding: .utf8) {
            if enableLogs {
                print("Current visitor profile: \(string)")
            }
        }
    }
}

enum TealiumError: Error {
    case noAudiencesReturned
}
