//
//  AppDelegate.swift
//  TealiumFirebaseExample
//
//  Created by Christina Sund on 7/18/19.
//  Copyright © 2019 Christina. All rights reserved.
//

import UIKit

var tealiumTraceId = ""

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate {

    var window: UIWindow?

    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplication.LaunchOptionsKey: Any]?) -> Bool {
        
        #if compiler(>=5.1)
        if #available(iOS 13.0, *) {
            // Always adopt a light interface style.
            window?.overrideUserInterfaceStyle = .light
        }
        #endif
        
        TealiumHelper.shared.start()
        //TealiumHelper.shared.start()
        TealiumHelper.joinTrace(traceId: tealiumTraceId)
        //TealiumHelper.joinTrace(traceId: "00553")
        return true
    }

    func applicationWillResignActive(_ application: UIApplication) { }

    func applicationDidEnterBackground(_ application: UIApplication) {
        TealiumHelper.killTrace(traceId:  tealiumTraceId)
        //TealiumHelper.killTrace(traceId: "00553")
        //TealiumHelperSwift.leaveTrace()
        //TealiumHelper.leaveTrace()
    }

    func applicationWillEnterForeground(_ application: UIApplication) { }

    func applicationDidBecomeActive(_ application: UIApplication) { }

    func applicationWillTerminate(_ application: UIApplication) { }


}

